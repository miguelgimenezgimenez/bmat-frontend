const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");


module.exports = {
  context: `${__dirname}/src`,
  entry: "./index.js",
  output: {
    path: path.join(__dirname, "dist"),
    publicPath: "/",
    filename: "bundle.js"
  },
  resolve: {
    extensions: [".js", ".json"],
    alias: {
      "./assets": `${__dirname}/src/assets`
    },
  },
  stats: {
    colors: true,
    reasons: true,
    chunks: true
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      }
    ]
  },
  devServer: {
    historyApiFallback: true
  },
  devtool: "eval-source-map",

  plugins: [
    new HtmlWebpackPlugin({
      template: __dirname + "/www/index.html"
    }),
    new webpack.DefinePlugin({
      "process.env": { NODE_ENV: `"${process.env.NODE_ENV || "development"}"` }
    })

  ]
};
