import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";

import { BrowserRouter } from "react-router-dom";

import { MuiThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";


import theme from "./theme";
import App from "./App";
import store from "./utils/store";


const render = () => (
  ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
        <MuiThemeProvider theme={theme}>
          <CssBaseline />
          <App />
        </MuiThemeProvider>
      </BrowserRouter>
    </Provider>,
    document.getElementById("app"))
);
render();

