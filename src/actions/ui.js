
export const open = (dispatch, componentType,value) =>
  dispatch({type: "UI_OPEN", componentType,value });

export const updateRecord = (dispatch, currentIndex, selectedMatch) =>
  dispatch({type: "SELECT_MATCH", currentIndex,selectedMatch });

export const close = (dispatch, componentType) =>
  dispatch({type: "UI_CLOSE", componentType });

export const setUploadFile = (dispatch, file) =>
  dispatch({type: "SET_UPLOAD_FILE",file});

export const toggleSelected = (dispatch, selectedItemId) =>
  dispatch({type: "TOGGLE_SELECTED", selectedItemId});