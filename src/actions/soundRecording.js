import apiCall, { getOptions } from "../utils/api";

const prefix = "SOUND_RECORDING";

// APICALL WILL HANDLE ERRORS!!

export const getSoundRecordingInputs = (dispatch, params) => {
  dispatch({ type: "SOUND_RECORDING_LOADING" });

  const endpoint = params ? `soundrecordingInput?${params}` : "soundrecordingInput";
  apiCall(endpoint, {}, prefix)
    .then(data => {
      dispatch({ type: "SET_SOUND_RECORDING_INPUT_LIST", data });
    });
};


export const uploadSoundRecordings = (dispatch, data, endpoint) => {
  dispatch({ type: "SOUND_RECORDING_LOADING" });
  apiCall(endpoint, { method: "POST", body: data }, prefix)
    .then(res => {
      // Right now, it doesnt matter if we upload inputs or just db records the list we want to update is the same one
      getSoundRecordingInputs(dispatch);
    });
};

export const updateRecord = (dispatch, currentId, matchId, currentIndex) => {
  const options = getOptions("PUT",{matchId});
  apiCall(`soundrecordingInput/${currentId}`, options, prefix)
    .then(res => {

      // if an index is given just update the list slicing the array
      if(currentIndex >= 0) return dispatch({ type: "SOUND_RECORDING_INPUT_LIST_UPDATE", currentIndex });
      // otherwise make an api call
      getSoundRecordingInputs(dispatch);
    });
};

