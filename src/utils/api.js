
import config from "../../config";
import store from "../utils/store";

const { apiUrl } = config[process.env.NODE_ENV];

export const getOptions = (method, data) => {
  const headers = { "Content-Type": "application/json" };
  const body = JSON.stringify(data);
  return { method, body , headers };
};

const apiCall = (endpoint, options, prefix) => {
  // if i return fetch directly the ".then" on the actions will always be called not allowing errors to be caught correctly.
  return new Promise((resolve) => {
    fetch(`${apiUrl}${endpoint}`, options)
      .then(response => {
        if (!response.ok) throw new Error(response.statusText);
        resolve(response.json());
      })
      .catch((error) => {
        const { dispatch } = store;
        dispatch({
          type: prefix ? `${prefix}_ERROR` : "ERROR",
          error: error.message
        });
      });

  });

};

export default apiCall;
