import get from "lodash.get";

export const desc = (a, b, orderBy) => {
  const aValue = get(a,orderBy);
  const bValue = get(b,orderBy);
  if (bValue < aValue) {
    return -1;
  }
  if (bValue > aValue) {
    return 1;
  }
  return 0;

};
export const stableSort = (array, cmp) => {

  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });

  return stabilizedThis.map(el => el[0]);
};

export const getSorting = (order, orderBy) => {
  return order === "desc" ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
};
