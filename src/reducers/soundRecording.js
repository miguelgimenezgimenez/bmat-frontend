
const INITIAL_STATE = {
  error: null,
  inputList: [],
  loading: false,
  deleted: null
};

const setError = (state, error) => ({ ...state,
  error,
  loading: false
});

const setLoading = (state) => ({ ...state,
  loading: true
});

const setSoundRecordingInpuList = (state, list) => {
  return { ...state,
    inputList: list,
    loading: false,
    error: null
  };
};
const updateSoundRecordingInpuList = (state, currentIndex) => {
  const { inputList} = state;

  const deleted = inputList[currentIndex];
  const updatedList = inputList.slice(0,currentIndex).concat(inputList.slice(currentIndex + 1));
  return { ...state,
    inputList: updatedList,
    deleted,
    loading: false,
    error: null
  };
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
  case "SOUND_RECORDING_ERROR":

    return setError(state, action.error);

  case "SOUND_RECORDING_LOADING":
    return setLoading(state);

  case "SET_SOUND_RECORDING_INPUT_LIST":
    return setSoundRecordingInpuList(state, action.data);

  case "SOUND_RECORDING_INPUT_LIST_UPDATE":
    return updateSoundRecordingInpuList(state, action.currentIndex);

  default:
    return state;
  }
};
