import ui from "./ui";
import soundRecording from "./soundRecording";

export default {
  soundRecording,
  ui,
};
