const INITIAL_STATE = {
  file: null,
  selectedRows: {},
  sideMenu: false,
  currentIndex: null,
  selectedMatch: null,
  selectRecordDialog: false,
  uploadDialog: false,
  fileType: null,
  selectedRecordSnackBar: false
};

const close = (state, componentType,value) => ({
  ...state,
  [componentType]: false,
  ...value
});

const open = (state, componentType, value) => ({
  ...state,
  [componentType]: true,
  ...value
});

const setUploadFile = (state, file) => ({
  ...state,
  file
});

const toggleSelected = (state,selectedItemId) => {

  return {...state,
    selectedRows: {...state.selectedRows, [selectedItemId]: !state.selectedRows[selectedItemId]}
  };
};


export default (state = INITIAL_STATE, action) => {
  switch (action.type) {

  case "UI_CLOSE":
    return close(state, action.componentType, action.value);

  case "UI_OPEN":
    return open(state, action.componentType, action.value);

  case "SET_UPLOAD_FILE":
    return setUploadFile(state, action.file);

  case "TOGGLE_SELECTED":
    return toggleSelected(state, action.selectedItemId);

  default:
    return state;
  }
};
