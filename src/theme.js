import { createMuiTheme } from "@material-ui/core/styles";

const primary = "#114464";
const secondary = "#04BEA7";
const background = "#DCDCDC";
const paleGrey = "#f6f7fb";

const toolbarHeight = 64;
const theme = createMuiTheme({

  typography: {
    useNextVariants: true,
    fontFamily: [
      "Source Sans Pro",
      "Roboto",
      "sans-serif",
    ].join(","),
  },
  overrides: {

    MuiDialog: {
      paper: {
        padding: 20
      }
    },
    MuiTableRow: {
      root: {
        cursor: "pointer",
        backgroundColor: paleGrey,

      }
    },
    MuiToolbar: {
      root: {
        height: toolbarHeight,
      }
    }
  },

  appBarHeight: toolbarHeight,

  palette: {

    primary: {
      main: primary ,
      contrastText: "white"},
    secondary: {
      main: secondary,
      contrastText: "white"
    },
    background: {
      default: background,
      selected: paleGrey
    }
  }
});

export default theme;
