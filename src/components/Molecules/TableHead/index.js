import React from "react";
import PropTypes from "prop-types";

import TableHead from "@material-ui/core/TableHead";
import Tooltip from "@material-ui/core/Tooltip";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";



class SortingTableHead extends React.PureComponent {

  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };

  render () {
    const {columns, order, orderBy} = this.props;

    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox"/>
          {columns.map(columns => {
            return (
              <TableCell
                key={columns.childKey}
                sortDirection={orderBy === columns.childKey ? order : false}
              >
                <Tooltip
                  title="Sort"
                  placement={"bottom-start"}
                  enterDelay={300}
                >
                  <TableSortLabel
                    active={orderBy === columns.childKey}
                    direction={order}
                    onClick={this.createSortHandler(columns.childKey)}
                  >
                    {columns.label}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            );
          })}
        </TableRow>
      </TableHead>
    );
  }
}

SortingTableHead.propTypes = {
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  columns: PropTypes.arrayOf(PropTypes.shape({
    childKey: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired
  })).isRequired,
};


export default SortingTableHead;