import React from "react";
import PropTypes from "prop-types";
import { Button } from "@material-ui/core";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";

const NestedListItem = (
  {
    record,
    updateRecord,
    index,
    pathname
  }) => (

  <ListItem style={{backgroundColor: "white"}} button >

    <Button
      onClick={() => updateRecord(record, index)}
      variant="contained"

      color="secondary" >
      {pathname === "/matches" ? "UNDO" : "SELECT"}

    </Button>
    <ListItemText
      inset
      primary={`${record.artist} , ${record.title}`}
      secondary={`${record.isrc}, ${record.length} `} />

  </ListItem>
);

NestedListItem.propTypes = {
  record: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  updateRecord: PropTypes.func.isRequired,
  index: PropTypes.number,
  pathname: PropTypes.string.isRequired,
};

export default NestedListItem;