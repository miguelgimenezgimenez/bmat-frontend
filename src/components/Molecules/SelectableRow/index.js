import React from "react";
import get from "lodash.get";
import { PropTypes } from "prop-types";

import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";

const SelectableRow = ({
  row,
  hover,
  columns,
  leftItem,
  handleClick,
  classes}) => (

  <TableRow
    classes={classes}
    hover={hover}
    onClick={() => handleClick(row)}
    tabIndex={-1}

  >
    <TableCell style={{width: 120}} padding="checkbox">
      {leftItem}
    </TableCell>
    {columns.map(column => (
      <TableCell key={column.childKey} >
        {get(row,column.childKey)}
      </TableCell>))
    }
  </TableRow>
);


SelectableRow.propTypes = {
  row: PropTypes.object.isRequired,
  hover: PropTypes.bool,
  columns: PropTypes.array.isRequired,
  leftItem: PropTypes.object.isRequired,
  handleClick: PropTypes.func.isRequired,
  classes: PropTypes.object,
};
export default SelectableRow;