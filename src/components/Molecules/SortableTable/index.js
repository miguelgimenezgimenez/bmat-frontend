import React from "react";
import PropTypes from "prop-types";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { withStyles } from "@material-ui/core/styles";

import TableHead from "../../Molecules/TableHead";
import { getSorting, stableSort } from "../../../utils/helpers";
import style from "./style";



class SortableTable extends React.PureComponent {
  state = {
    order: "asc",
    orderBy: "",
    selected: [],
    page: 0,
    rowsPerPage: 10,
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = "desc";

    if (this.state.orderBy === property && this.state.order === "desc") {
      order = "asc";
    }

    this.setState({ order, orderBy });
  };


  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };


  render () {
    const { classes, rows, columns,children } = this.props;

    const { order, orderBy, selected, rowsPerPage, page } = this.state;

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

    return (
      <Paper className={classes.root}>
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <TableHead
              columns={columns}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onRequestSort={this.handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>

              {
                stableSort(rows, getSorting(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  //call render children function on each row
                  .map(children)
              }

              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={columns.length + 1} />
                </TableRow>
              )}

            </TableBody>
          </Table>
        </div>
        <TablePagination
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            "aria-label": "Previous Page",
          }}
          nextIconButtonProps={{
            "aria-label": "Next Page",
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    );
  }
}

SortableTable.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.func.isRequired,
  // CREATE CUSTOM PROPTYPE VALIDATOR
  rows: PropTypes.arrayOf(PropTypes.shape({
  })).isRequired,
  renderChildren: PropTypes.func,
  columns: PropTypes.arrayOf(PropTypes.shape({
    childKey: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
  })).isRequired,
};



export default withStyles(style)(SortableTable);
