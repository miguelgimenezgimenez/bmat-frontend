import React from "react";
import PropTypes from "prop-types";

import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";

import style from "./style";


const DialogMolecule = ({ open, title, children, classes, handleCloseDialog, save }) => (
  <Dialog
    className={classes.dialog}
    onClose={handleCloseDialog}
    aria-labelledby="customized-dialog-title"
    open={open}
  >
    <MuiDialogTitle id="customized-dialog-title" onClose={handleCloseDialog}>
      {title}
    </MuiDialogTitle>
    <MuiDialogContent>
      {children}
    </MuiDialogContent>
    <MuiDialogActions>
      <Button onClick={save} variant="contained" color="secondary">
        Save changes
      </Button>
      <Button onClick={handleCloseDialog} variant="contained" className={classes.cancelButton}>
        Cancel
      </Button>
    </MuiDialogActions>
  </Dialog>
);

DialogMolecule.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  handleCloseDialog: PropTypes.func.isRequired,
  save: PropTypes.func.isRequired,
};

export default withStyles(style)(DialogMolecule);