export default theme => ({
  root: {
    width: "100%",
    marginTop: 40,
  },
  child: {
    backgroundColor: "white",

  },
  nested: {
    width: "100%",
    paddingLeft: theme.spacing.unit * 4,
  },
  loader: {
    margin: "200px auto"

  }

});