import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { JamminLoader } from "react-loaders-spinners";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";

import style from "./style";
import { withRouter } from "react-router-dom";

import NestedListItem from "../NestedListItem";

const NestedList = ({
  classes,
  soundRecordingInputList,
  selectRow,
  selectedRows,
  loading,
  location,
  updateRecord}) => {


  const { pathname } = location;

  const matchesLocation = pathname === "/matches";

  if (loading) return (
    <JamminLoader className={classes.loader} pColor='#04BEA7'
    />);

  return (
    <div className={classes.root}>
      <List
        component="nav"
      >
        {soundRecordingInputList.map((item,index) => (
          <React.Fragment key={item.id}>
            <ListItem button onClick={() => selectRow(item)}>
              <ListItemText

                primary={`${item.artist} , ${item.title}`}
                secondary={`${item.isrc}, ${item.length} `} />

              {
                !!selectedRows[item.id] ? <ExpandLess /> : <ExpandMore />
              }
            </ListItem>
            <Collapse in={selectedRows[item.id]} timeout="auto" unmountOnExit>
              {
                matchesLocation ?
                  <NestedListItem
                    pathname={pathname}
                    index={index}
                    updateRecord={updateRecord}
                    record={item.selectedCandidate}
                    classes={classes}
                  /> :
                  item.matches.map((record) => (
                    <NestedListItem
                      pathname={pathname}
                      key={record.id}
                      index={index}
                      updateRecord={updateRecord}
                      record={record}
                      classes={classes}
                    />))
              }

            </Collapse>
          </React.Fragment>
        ))
        }

      </List>
    </div>
  );
};


NestedList.propTypes = {
  classes: PropTypes.object.isRequired,
  soundRecordingInputList: PropTypes.array.isRequired,
  selectRow: PropTypes.func.isRequired,
  updateRecord: PropTypes.func.isRequired,
  selectedRows: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,

};

export default withRouter(withStyles(style)(NestedList));
