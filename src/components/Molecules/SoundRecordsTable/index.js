import React, { PureComponent } from "react";
import { JamminLoader } from "react-loaders-spinners";
import { withRouter } from "react-router-dom";
import { PropTypes } from "prop-types";

import { Button } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";

import SortableTable from "../../Molecules/SortableTable";
import SelectableRow from "../../Molecules/SelectableRow";
import ExpandButton from "../../Atoms/ExpandButton";
import style from "./style";


// These will be the columns for the sortable table, the childKey field will be the key of the object allowing to get nested fields as it will access the field through lodash-get
const columns = [
  { childKey: "artist", label: "Artist", type: "string" },
  { childKey: "title", label: "Title", type: "string" },
  { childKey: "isrc", label: "ISRC", type: "string" },
  { childKey: "length", label: "Duration", type: "string" }
];

const NestedTableRow = ({record, classes, updateRecord, index, pathname}) => (

  <SelectableRow
    classes={{ root: classes.dbRecordings }}
    key={record.id}
    row={record}
    columns={columns}
    handleClick={() => {}}
    leftItem={
      <Button
        onClick={() => updateRecord(record, index)}
        variant="contained"
        className={classes.selectButton}
        color="secondary" >
        {pathname === "/matches" ? "UNDO" : "SELECT"}
      </Button>}
  />
);


class SoundRecordsTable extends PureComponent {

  renderRowChildren =(row, index,) => {
    const { classes, location, updateRecord } = this.props;
    const {pathname } = location;

    let updateRecordCallback = updateRecord;
    let itemToMap = row.matches;

    if (pathname === "/matches") {
      updateRecordCallback = () => updateRecord(row, index);
      itemToMap = [row.selectedCandidate];
    }

    return itemToMap.map(record => (
      <NestedTableRow
        pathname={pathname}
        key={record.id}
        index={index}
        updateRecord={updateRecordCallback}
        record={record}
        classes={classes}
      />
    ));

  }

  render () {

    const {
      classes,
      soundRecordingInputList,
      selectRow,
      selectedRows,
      loading,
    } = this.props;

    if (loading) return (
      <JamminLoader className={classes.loader} pColor='#04BEA7'
      />);

    return (
      <SortableTable
        rows={soundRecordingInputList}
        columns={columns}
      >
        {
          (row,index) => (
            <React.Fragment key={row.id} >
              <SelectableRow
                hover
                row={row}
                columns={columns}
                selectedId={selectedRows[row.id]}
                handleClick={selectRow}
                leftItem={
                  <ExpandButton
                    className={classes.expandButton}
                    more={selectedRows[row.id]}
                  />}
              />
              {
                !!selectedRows[row.id] && this.renderRowChildren(row, index)
              }
            </React.Fragment>
          )
        }
      </SortableTable>

    );
  }
}

SoundRecordsTable.propTypes = {
  classes: PropTypes.object.isRequired,
  soundRecordingInputList: PropTypes.array.isRequired,
  selectRow: PropTypes.func.isRequired,
  updateRecord: PropTypes.func.isRequired,
  selectedRows: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,

};

export default withRouter(withStyles(style)(SoundRecordsTable));