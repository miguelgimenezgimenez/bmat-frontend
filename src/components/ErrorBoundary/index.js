import React from "react";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";

import { connect } from "react-redux";


class ErrorBoundary extends React.Component {
  constructor (props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch (error, info) {
    console.error(error, info);
  }
  static getDerivedStateFromError (error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }


  render () {
    const { soundRecordsError } = this.props;
    const { hasError } = this.state;
    if (hasError || soundRecordsError) {
      // You can render any custom fallback UI
      return <Typography variant="h4" color="error">AN ERROR OCCURRED {soundRecordsError} </Typography>;
    }

    return this.props.children;
  }
}

const mapStateToProps = (state) => ({
  soundRecordsError: state.soundRecording.error
});

ErrorBoundary.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array
  ]).isRequired
};


export default connect(mapStateToProps)(ErrorBoundary);