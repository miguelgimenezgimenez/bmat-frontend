import React from "react";
import { connect } from "react-redux";
import get from "lodash.get";

import Dialog from "../../Molecules/Dialog";
import * as uiActions from "../../../actions/ui";
import * as soundRecordingActions from "../../../actions/soundRecording";

class SelectRecordDialog extends React.PureComponent {

  handleCloseDialog = () => {
    uiActions.close(this.props.dispatch, "selectRecordDialog", {selectedMatch: null});
  }

  updateRecord = () => {
    const { dispatch, selectedMatch,currentIndex, soundRecordingInputList} = this.props;
    this.handleCloseDialog();
    soundRecordingActions.updateRecord(dispatch,soundRecordingInputList[currentIndex].id, selectedMatch.id, currentIndex);
  }

  render () {

    const { selectRecordDialog,soundRecordingInputList, selectedMatch, currentIndex} = this.props;
    const current = soundRecordingInputList[currentIndex];


    return (
      <Dialog
        title="Selected Matches"
        save={this.updateRecord}
        handleCloseDialog={this.handleCloseDialog}
        open={selectRecordDialog}
      >
        <React.Fragment>
        The selected record for
          <p/> <strong>
            {` ${get(current,"artist","")}  
              ${get(current,"title","")}  `}
          </strong>
          <p/>is :
          <p/><strong>
            {` ${get(selectedMatch,"artist","")}  
              ${get(selectedMatch,"title","")}`} </strong>

        </React.Fragment>
      </Dialog>
    );
  }
}

const mapStateToProps = state => ({
  soundRecordingInputList: state.soundRecording.inputList,
  selectRecordDialog: state.ui.selectRecordDialog,
  selectedMatch: state.ui.selectedMatch,
  currentIndex: state.ui.currentIndex,
});

export default connect(mapStateToProps)(SelectRecordDialog);