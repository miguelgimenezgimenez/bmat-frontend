import React from "react";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";

import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { withStyles } from "@material-ui/core/styles";
import { IconButton } from "@material-ui/core";

import style from "./style";

const linkWrapper = (Component) => props => (
  <NavLink to={props.to} exact activeStyle={{ backgroundColor: "#04BEA7", zIndex: 100 }} >
    <Component {...props} />
  </NavLink>
);

const buttonWrapper = (Component, classes) => props => (
  <IconButton classes={{root: classes.iconButton}} onClick={() => props.cb(props.to)}>
    <Component {...props} />
  </IconButton>
);

const ListItemWrapper = ({text, icon, classes}) =>
  <ListItem>
    {icon && <ListItemIcon>
      {icon}
    </ListItemIcon>}
    <ListItemText primary={text} />
  </ListItem>;


const SideBar = ({
  classes,
  items,
  open,
  cb,
  handleDrawerToggle
}) => {

  return (
    <nav className={classes.drawer}>
      <Drawer
        variant="temporary"
        open={open}
        onClose={handleDrawerToggle}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        {
          items.map(item => {
            const Component = item.type === "link" ? linkWrapper(ListItemWrapper) : buttonWrapper(ListItemWrapper, classes);

            return (
              <React.Fragment key={item.text} >
                <Component {...item} cb={cb} />
                <Divider />
              </React.Fragment>
            ); })
        }
      </Drawer>
    </nav>


  );
};


SideBar.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withStyles(style)(SideBar);