const drawerWidth = 280;

export default theme => ({
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  iconButton: {
    padding: 0
  },
  listItem: {
    height: 10
  },
  drawerPaper: {
    width: drawerWidth,
  },

});
