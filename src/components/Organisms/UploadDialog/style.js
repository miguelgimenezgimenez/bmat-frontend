export default theme => ({
  baseStyle: {
    width: 400,
    height: 300,
    padding: 40,
    margin: "20px 40px",
    borderWidth: 4,
    borderColor: theme.palette.secondary.main,
    borderStyle: "dashed",
    borderRadius: 20,
    cursor: "pointer"
  },
  activeStyle: {
    borderStyle: "solid",
    borderColor: "#6c6",
    backgroundColor: "#eee"
  },
  rejectStyle: {
    borderStyle: "solid",
    borderColor: "red",
    backgroundColor: "#eee"
  },
  cancelButton: {
    backgroundColor: "red",
    color: "white"

  }
});