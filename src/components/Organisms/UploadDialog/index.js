import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Dropzone from "react-dropzone";
import classNames from "classnames";

import Typography from "@material-ui/core/Typography";

import Dialog from "../../Molecules/Dialog";
import * as uiActions from "../../../actions/ui";
import * as soundRecordingActions from "../../../actions/soundRecording";
import style from "./style";


class UploadDialog extends React.PureComponent {

  onDrop = ([file]) => {
    uiActions.setUploadFile(this.props.dispatch,file);
  }

  handleCloseDialog = () => {
    uiActions.close(this.props.dispatch, "uploadDialog", {fileType: null});
  }

  uploadFile = () => {
    const { dispatch, file, fileType } = this.props;
    const data = new FormData();
    data.append("csv_file", file);
    const endpoint = fileType === "inputs" ? "soundrecordingInput" : "soundrecording";

    this.handleCloseDialog();
    soundRecordingActions.uploadSoundRecordings(dispatch, data, endpoint);
  }

  render () {

    const { uploadDialog, fileType, file, classes } = this.props;

    return (
      <Dialog
        handleCloseDialog={this.handleCloseDialog}
        save={this.uploadFile}
        open={uploadDialog}
        title= {`Upload ${fileType} to Database`}
      >

        <Dropzone
          onDrop={this.onDrop}
          accept=".csv">
          {({ getRootProps, getInputProps, isDragActive, isDragReject}) => {
            return (
              <div
                {...getRootProps()}
                className={classNames(classes.baseStyle, {
                  [classes.activeStyle]: isDragActive,
                  [classes.rejectStyle]: isDragReject,
                })}
              >
                <input {...getInputProps()} />
                <Typography variant="h6" color="primary" noWrap>
                  {file ? file.name : "Dragfiles or Click here..."}
                </Typography>
                {isDragReject && <Typography variant="h6" color="primary" >Unsupported file type...</Typography>}
              </div>
            );
          }}
        </Dropzone>

      </Dialog>
    );
  }
}

const mapStateToProps = state => ({
  uploadDialog: state.ui.uploadDialog,
  fileType: state.ui.fileType,
  file: state.ui.file,
});

export default connect(mapStateToProps)(withStyles(style)(UploadDialog));