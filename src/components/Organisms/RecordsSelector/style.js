export default theme => ({

  dbRecordings: {
    backgroundColor: "white",
  },
  expandButton: {
    marginRight: 25
  },
  selectButton: {
    fontSize: 10
  },
  loader: {
    margin: "200px auto"
  }
});