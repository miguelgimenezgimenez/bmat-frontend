import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import Hidden from "@material-ui/core/Hidden";

import * as soundRecordingActions from "../../../actions/soundRecording";
import * as uiActions from "../../../actions/ui";
import ErrorBoundary from "../../ErrorBoundary";
import SoundRecordsTable from "../../Molecules/SoundRecordsTable";
import NestedList from "../../Molecules/NestedList";

class RecordsSelector extends PureComponent {

  componentDidMount = () => {
    const { dispatch, matches } = this.props;
    const query = matches ? "matched=true" : null;
    soundRecordingActions.getSoundRecordingInputs(dispatch,query
    );
  }

  selectRecord = (selectedMatch, currentIndex) => {

    const { dispatch, matches,soundRecordingInputList } = this.props;
    const value = { selectedMatch, currentIndex };

    if (matches) {
      const current = soundRecordingInputList[currentIndex];
      soundRecordingActions.updateRecord(dispatch, current.id, null, currentIndex);
      uiActions.close(dispatch, "selectedRecordSnackBar");
    } else {
      uiActions.open(dispatch, "selectRecordDialog", value);
    }
  }

  selectRow = ({ id }) => {
    const { dispatch } = this.props;
    uiActions.toggleSelected(dispatch, id);
  }

  render () {
    const {
      soundRecordingInputList,
      selectedRows,
      loading
    } = this.props;

    return (
      <ErrorBoundary>
        <Hidden only={["sm", "md","xs"]}>
          <SoundRecordsTable
            loading={loading}
            selectRow={this.selectRow}
            soundRecordingInputList={soundRecordingInputList}
            selectedRows={selectedRows}
            updateRecord={this.selectRecord}
          />
        </Hidden>
        <Hidden only={["lg", "xl"]}>
          <NestedList
            loading={loading}
            selectRow={this.selectRow}
            soundRecordingInputList={soundRecordingInputList}
            selectedRows={selectedRows}
            updateRecord={this.selectRecord}
          />
        </Hidden>

      </ErrorBoundary>
    );
  }
}

const mapStateToProps = state => ({
  selectedRows: state.ui.selectedRows,
  loading: state.soundRecording.loading,
  soundRecordingInputList: state.soundRecording.inputList,
  error: state.soundRecording.error,

});

export default withRouter(connect(mapStateToProps)(RecordsSelector));