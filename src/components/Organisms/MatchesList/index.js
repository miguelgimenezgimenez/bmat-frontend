import React, { PureComponent } from "react";
import { connect } from "react-redux";

import Hidden from "@material-ui/core/Hidden";

import * as soundRecordingActions from "../../../actions/soundRecording";
import * as uiActions from "../../../actions/ui";
import ErrorBoundary from "../../ErrorBoundary";
import SoundRecordsTable from "../../Molecules/SoundRecordsTable/index";
import NestedList from "../../Molecules/NestedList";




class MatchesList extends PureComponent {

  componentDidMount = () => {
    const { dispatch } = this.props;
    soundRecordingActions.getSoundRecordingInputs(dispatch,
      "matched=true");
  }

  selectedRecordUndo = (selected, index) => {
    const { dispatch } = this.props;
    soundRecordingActions.updateRecord(dispatch, selected.id, null, index);
    uiActions.close(dispatch, "selectedRecordSnackBar");
  }

  selectRow = ({ id }) => {
    const { dispatch } = this.props;
    uiActions.toggleSelected(dispatch, id);
  }

  render () {
    const {
      soundRecordingInputList,
      selectedRows,
      loading
    } = this.props;

    return (
      <ErrorBoundary>
        <Hidden only={["sm", "md","xs"]}>

          <SoundRecordsTable
            loading={loading}
            selectRow={this.selectRow}
            soundRecordingInputList={soundRecordingInputList}
            selectedRows={selectedRows}
            updateRecord={this.selectedRecordUndo}
          />
        </Hidden>
        <Hidden only={["lg", "xl"]}>
          <NestedList
            loading={loading}
            selectRow={this.selectRow}
            soundRecordingInputList={soundRecordingInputList}
            selectedRows={selectedRows}
            updateRecord={this.selectedRecordUndo}
          />
        </Hidden>
      </ErrorBoundary>
    );
  }
}

const mapStateToProps = state => ({
  selectedRows: state.ui.selectedRows,
  loading: state.soundRecording.loading,
  soundRecordingInputList: state.soundRecording.inputList,
  error: state.soundRecording.error,

});

export default connect(mapStateToProps)(MatchesList);