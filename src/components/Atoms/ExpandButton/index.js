import React from "react";
import PropTypes from "prop-types";

import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";

import { IconButton } from "@material-ui/core";

const ExpandButton = ({more, className}) => <IconButton className={className}> {more ? <ExpandMore /> : <ExpandLess />}</IconButton>;

ExpandButton.propTypes = {
  more: PropTypes.bool,
};

export default ExpandButton;