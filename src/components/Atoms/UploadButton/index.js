import React from "react";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import { Button } from "@material-ui/core";

const UploadButton = ({handleClick, text}) => (
  <Button
    onClick={handleClick}
    color="secondary"
    variant="contained">
    {text}
    <CloudUploadIcon style={{marginLeft: 5}}/>
  </Button>);



// TODO ADD PROPTYPES
export default UploadButton;