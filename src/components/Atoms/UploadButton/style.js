export default (theme) => ({
  rightIcon: {
    marginLeft: theme.spacing.unit * 2,
  },
  uploadButtons: {
    width: 420,
    display: "flex",
    justifyContent: "space-between"
  }
});