import React, { Component } from "react";

import { withStyles } from "@material-ui/core";
import { connect } from "react-redux";
import { NavLink, withRouter } from "react-router-dom";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";

import UploadDialog from "../Organisms/UploadDialog";
import SelectRecordDialog from "../Organisms/SelectRecordDialog";
import * as uiActions from "../../actions/ui";
import UploadButton from "../Atoms/UploadButton";
import SnackBar from "../Molecules/SnackBar";
import * as soundRecordingActions from "../../actions/soundRecording";
import style from "./style";
import SideBar from "../Organisms/SideBar";

const sideBarItems = [
  {type: "link", to: "/",text: "match Records"},
  {type: "link", to: "/matches", text: "Unmatch Records"},
  {type: "button", to: "inputs", text: "Upload Inputs",icon: <CloudUploadIcon/>},
  {type: "button", to: "records", text: "Upload Records",icon: <CloudUploadIcon/>},
];

class Layout extends Component {


  componentDidUpdate (prevProps, prevState) {
    const { dispatch, deleted } = this.props;

    const itemUpdated = deleted !== prevProps.deleted;

    if(itemUpdated) {
      uiActions.open(dispatch, "selectedRecordSnackBar");
    }
  }


  closeSnackBar=() => {
    const { dispatch } = this.props;
    uiActions.close(dispatch, "selectedRecordSnackBar");
  }

  handleOpenDialog = (value) => {
    uiActions.open(this.props.dispatch, "uploadDialog", { fileType: value });
  }

  handleDrawerToggle = () => {
    const { sideMenu } = this.props;
    sideMenu ? uiActions.close(this.props.dispatch, "sideMenu") :
      uiActions.open(this.props.dispatch, "sideMenu");
  }


  selectedRecordUndo = () => {
    const { dispatch, deleted} = this.props;
    soundRecordingActions.updateRecord(dispatch,deleted.id, null);
    uiActions.close(dispatch, "selectedRecordSnackBar");
  }


  render () {
    const { classes, selectedRecordSnackBar, sideMenu } = this.props;

    return (
      <React.Fragment >

        <SelectRecordDialog />

        <UploadDialog />

        <SideBar
          cb={this.handleOpenDialog}
          open={sideMenu}
          items={sideBarItems}
          handleDrawerToggle={this.handleDrawerToggle}
        />

        <SnackBar
          handleClose={this.closeSnackBar}
          message="Match Updated"
          open={selectedRecordSnackBar}
          undo={this.selectedRecordUndo} />

        <AppBar position="fixed">
          <Toolbar className={classes.toolbar}>
            <div className={classes.nav} >

              {["/","/matches"].map(to =>
                <NavLink key={to} activeStyle={{ color: "#04BEA7" }}
                  className={classes.link} exact to={to}>
                  <Typography color="inherit" >
                    { to === "/" ? "Match Records" : "Unmatch records"}
                  </Typography>
                </NavLink>) }

            </div>

            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerToggle}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>

            <div className={classes.uploadButtons} >
              <UploadButton
                text="Upload Records"
                handleClick={() => this.handleOpenDialog("records")} />
              <UploadButton
                text="Upload Inputs"
                handleClick={() => this.handleOpenDialog("inputs")} />
            </div>

          </Toolbar>
        </AppBar>

        <div className={classes.main}>
          {this.props.children}
        </div>

      </React.Fragment>
    );
  }
}


const mapStateToProps = (state) => ({
  selectedRecordSnackBar: state.ui.selectedRecordSnackBar,
  currentIndex: state.ui.currentIndex,
  sideMenu: state.ui.sideMenu,
  soundRecordingInputList: state.soundRecording.inputList,
  deleted: state.soundRecording.deleted
});

export default withRouter(connect(mapStateToProps)(withStyles(style)(Layout)));