export default (theme) => ({

  main: {
    display: "flex",
    marginTop: theme.appBarHeight,
    justifyContent: "space-between"
  },
  drawer: {
    display: "block"
  },
  appBarLinks: {
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
  },

  toolbar: {
    display: "flex",
    justifyContent: "space-between"
  },
  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up("md","lg")]: {
      display: "none",
    },
  },
  uploadButtons: {
    width: 420,
    display: "flex",
    justifyContent: "space-between",
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
  },
  nav: {
    display: "flex",
    width: 260,
    justifyContent: "space-around",
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
  },
  link: {
    textDecoration: "none",
    color: "white"
  },
  active: {
    color: theme.palette.secondary
  }

});