import React from "react";
import { withStyles } from "@material-ui/core/styles";

import style from "./style";

import RecordsSelector from "../../Organisms/RecordsSelector";

const Matches = ({ classes }) => {

  return (
    <div className={classes.matchContainer}>
      <RecordsSelector matches={true} />
    </div>
  );
};



export default (withStyles(style)(Matches));