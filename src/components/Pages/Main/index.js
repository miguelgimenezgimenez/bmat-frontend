import React from "react";
import { withStyles } from "@material-ui/core/styles";

import style from "./style";
import RecordsSelector from "../../Organisms/RecordsSelector";


const Main = ({ classes }) => {

  return (
    <div className={classes.mainContainer}>
      <RecordsSelector />
    </div>
  );
};



export default (withStyles(style)(Main));