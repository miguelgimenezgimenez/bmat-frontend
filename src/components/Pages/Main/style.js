export default theme => ({
  mainContainer: {
    width: "100%",
    textAlign: "center",
    padding: 24,
  } ,
  dbRecordings: {
    backgroundColor: theme.palette.palegray
  }
});