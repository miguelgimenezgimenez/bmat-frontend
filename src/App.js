import React from "react";
import { Route } from "react-router-dom";
import Main from "./components/Pages/Main";
import Matches from "./components/Pages/Matches";

import Layout from "./components/Layout";
import "./style.css";

export default () => {
  return (
    <Layout>
      <Route exact path="/" component={Main}/>
      <Route exact path="/matches" component={Matches}/>
    </Layout>

  );
};

