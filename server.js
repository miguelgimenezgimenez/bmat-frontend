const express = require("express");
const cors = require("cors");

const history = require("connect-history-api-fallback");

const port = process.env.PORT || 8080;
const app = express();
app.use(history());
app.use(cors());
app.use(express.static("dist"));

app.get("*", (req, res) => {

  res.render("index.html");
});

app.use((error, req, res, next) => {
  res.status(500).send({ error: error.message });
});

app.listen(port, () => {
  // eslint-disable-next-line
  console.log('Express running on http://localhost:8080')
});
